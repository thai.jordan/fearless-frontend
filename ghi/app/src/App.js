import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import { BrowserRouter } from "react-router-dom";
import { Routes } from "react-router-dom";
import { Route } from "react-router-dom";
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
        <Routes>
          <Route path="mainpage/" index element={<MainPage />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="attendees/new" element={<AttendConferenceForm />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="presentation/new" element={<PresentationForm />} />
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
        </Routes>
    </BrowserRouter>
  );
}







export default App;
